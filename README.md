# OpenLicht Binding

Currently, the additional package `org.eclipse.smarthome.io.transport.mqtt` is needed for this binding to work.
It can be exported as a plugin from the OpenHAB Eclipse.

This binding is used as a collection of bindings developed with the project [OpenLicht](http://openlicht.de).
All bindings use MQTT for communication between the device and openHAB.

## Supported Things

Name | Description | Handler | Sub topic
-----|-------------|---------|----------
SkyWriterHAT | [Product-Page](https://shop.pimoroni.com/products/skywriter-hat) - [Documentation](http://docs.pimoroni.com/skywriter/#) | [SkyWriterHATHandler](https://git-st.inf.tu-dresden.de/OpenLicht/org.openhab.binding.openlicht/blob/master/src/main/java/org/openhab/binding/openlicht/handler/SkyWriterHATHandler.java) | `skywriter`
Polar M600 | [Product-Page](https://www.polar.com/de/produkte/sport/M600-Fitness-Smartwatch) | [PolarM600Handler](https://git-st.inf.tu-dresden.de/OpenLicht/org.openhab.binding.openlicht/blob/master/src/main/java/org/openhab/binding/openlicht/handler/PolarM600Handler.java) | `polar`
Moto 360 | from CyPhyMan, [Product-Page](https://www.motorola.com.au/products/moto-360) | [Moto360Handler](https://git-st.inf.tu-dresden.de/OpenLicht/org.openhab.binding.openlicht/blob/master/src/main/java/org/openhab/binding/openlicht/handler/Moto360Handler.java) | `moto360`
Samsung S6 | Could actually be any smart phone | [SamsungS6Handler](https://git-st.inf.tu-dresden.de/OpenLicht/org.openhab.binding.openlicht/blob/master/src/main/java/org/openhab/binding/openlicht/handler/SamsungS6Handler.java) | `samsung`¹

¹:warning: The topic is currently set in `AbstractSmartphoneHandler`. This is a bug.

## Discovery

Not implemented (yet).

## Binding Configuration

*None*

## Thing Configuration

All bindings share the same configuration, with the following options (copied from `ESH-INF/config/config.xml`:

Name | Label | Description | Default
-----|-------|-------------|--------
`brokerName` | Broker Name | Name of the broker as defined in the &lt;broker&gt;.url in services/mqtt.cfg. See the MQTT Binding for more information on how to configure MQTT broker connections. | `mosquitto`²
`base-topic` | Base-Topic | Base topic for publishing updates. Do not include a trailing slash. | `sensors`
`unsupported-category-reset` | MQTT Unsupported Category Reset Timeout (optional) | Timeout in seconds to log again unsupported MQTT categories. Set to zero to disable (default). | `0`

²:warning: This must match the configured MQTT broker within openHAB, see `services/mqtt.cfg`.

## Channel Types

Type-Name | Kind | Label | Description | Category
----------|------|-------|-------------|---------
`flick-type` | Text | Last Flick | Last Flick detected (and its direction) | Motion
`brightness-type` | Number | Brightness | Brightness (Lux) | Light
`acceleration-type` | Number | Acceleration | Acceleration in one direction in meters per second. | Motion
`rotation-type` | Number | Rotation | Rotation around one axis (unitless). | Motion
`activity-type` | String | Activity | Current recognized activity, one of Runnning, Walking, Resting, Unknown. | - 
`heart-rate-type` | Number | Heart Rate | Heart rate in beats per minute. | -
`steps-type` | Number | Steps | Steps run today. | -

## Channels

Supported by Binding | Type-Name | Id | Label
---------------------|-----------|----|------
SkyWriterHAT | `flick-type` | `flick` | -
Polar M600, Moto 360, Samsung S6 | `brightness-type` | `brightness` | -
Polar M600, Moto 360 | `acceleration-type` | `acceleration-x` | Acceleration X
Polar M600, Moto 360 | `acceleration-type` | `acceleration-y` | Acceleration Y
Polar M600, Moto 360 | `acceleration-type` | `acceleration-z` | Acceleration Z
Polar M600, Moto 360, Samsung S6 | `rotation-type` | `rotation-x` | Rotation X
Polar M600, Moto 360, Samsung S6 | `rotation-type` | `rotation-y` | Rotation Y
Polar M600, Moto 360, Samsung S6 | `rotation-type` | `rotation-z` | Rotation Z
Polar M600, Moto 360 | `activity-type` | `activity` | -
Polar M600, Moto 360 | `heart-rate-type` | `heart-rate` | -
Polar M600, Moto 360 | `steps-type` | `steps` | -

## Data flow

[AndroidSensorSharing repository](https://git-st.inf.tu-dresden.de/OpenLicht/AndroidSensorSharing)

![material/dataflow.png](material/dataflow.png)
