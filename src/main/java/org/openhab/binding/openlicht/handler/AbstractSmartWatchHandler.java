/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.openlicht.handler;

import static org.openhab.binding.openlicht.BindingConstants.*;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link AbstractSmartWatchHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author René Schöne - Initial contribution
 */
public abstract class AbstractSmartWatchHandler extends AbstractMqttHandler {

    private final Logger logger = LoggerFactory.getLogger(AbstractSmartWatchHandler.class);

    private static Set<String> seenUnsupportedCategories;

    public AbstractSmartWatchHandler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing, configurationHolder);
        maybeSetupStatics();
        Integer timeout = getConfigValueAsInteger(CONFIG_TIMEOUT_MQTT_UNSUPPORTED_CATEGORIES);
        if (timeout != null && timeout > 0) {
            Runnable command = new Runnable() {

                @Override
                public void run() {
                    seenUnsupportedCategories.clear();
                }
            };
            if (executor != null) {
                executor.scheduleAtFixedRate(command, 0, timeout, TimeUnit.SECONDS);
            } else {
                logger.warn("Did not schedule reset thread for unsupported categories.");
            }
        }
    }

    private synchronized void maybeSetupStatics() {
        if (seenUnsupportedCategories == null) {
            seenUnsupportedCategories = new HashSet<>();
        }
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        if (command instanceof RefreshType) {
            return;
        }
        logger.debug("Got command for read-only thing: {} {}", channelUID.getAsString(), command.toFullString());
    }

    @Override
    public void processMessage(String topic, byte[] payload) {
        // switch category of message (last part of topic after last slash)
        String category = topic.substring(getTopicLength());
        if (byteBaseMessages) {
            ByteBuffer buffer = ByteBuffer.wrap(payload);
            DoubleBuffer doubleBuffer;
            switch (category) {
                case MQTT_CATEGORY_BRIGHTNESS:
                    updateState(CHANNEL_BRIGHTNESS, new DecimalType(buffer.getFloat()));
                    break;
                case MQTT_CATEGORY_ACCELERATION:
                    doubleBuffer = buffer.asDoubleBuffer();
                    updateState(CHANNEL_ACCELERATION_X, new DecimalType(doubleBuffer.get(0)));
                    updateState(CHANNEL_ACCELERATION_Y, new DecimalType(doubleBuffer.get(1)));
                    updateState(CHANNEL_ACCELERATION_Z, new DecimalType(doubleBuffer.get(2)));
                case MQTT_CATEGORY_ROTATION:
                    doubleBuffer = buffer.asDoubleBuffer();
                    updateState(CHANNEL_ROTATION_X, new DecimalType(doubleBuffer.get(0)));
                    updateState(CHANNEL_ROTATION_Y, new DecimalType(doubleBuffer.get(1)));
                    updateState(CHANNEL_ROTATION_Z, new DecimalType(doubleBuffer.get(2)));
                default:
                    logUnsupportedCategory(category);
            }
        } else {
            String message = new String(payload);
            switch (category) {
                case MQTT_CATEGORY_BRIGHTNESS:
                    updateState(CHANNEL_BRIGHTNESS, new DecimalType(Double.parseDouble(message)));
                    break;
                case MQTT_CATEGORY_ACCELERATION:
                    MqttUtils.handleAcceleration(message, this::updateState);
                    break;
                case MQTT_CATEGORY_ROTATION:
                    MqttUtils.handleRotation(message, this::updateState);
                    break;
                default:
                    logUnsupportedCategory(category);
            }
        }
    }

    private void logUnsupportedCategory(String category) {
        if (seenUnsupportedCategories.add(category)) {
            logger.warn("Category not supported: {}", category);

        }
    }

    @Override
    protected boolean subscribeSubTopics() {
        return true;
    }

}
