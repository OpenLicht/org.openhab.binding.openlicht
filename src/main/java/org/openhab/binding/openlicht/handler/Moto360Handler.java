package org.openhab.binding.openlicht.handler;

import org.eclipse.smarthome.core.thing.Thing;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;

public class Moto360Handler extends AbstractSmartWatchHandler {

    public Moto360Handler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing, configurationHolder);
    }

    @Override
    protected String getSubTopic() {
        return "moto360";
    }

}
