package org.openhab.binding.openlicht.handler;

import static org.openhab.binding.openlicht.BindingConstants.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.smarthome.core.library.types.DecimalType;

public class MqttUtils {

    public static Map<String, DecimalType> handleSimpleJsonMessage(final String message) {
        Map<String, DecimalType> result = new HashMap<>();
        String content = message.substring(1, message.length() - 1);
        String[] tokens = content.split(",");
        for (String tok : tokens) {
            String[] keyValue = tok.split("=");
            String key = keyValue[0].trim();
            DecimalType value = new DecimalType(Double.parseDouble(keyValue[1].trim()));
            result.put(key, value);
        }
        return result;
    }

    public static void handleRotation(String message, UpdateState updateState) {
        // state has the form "{z=$FLOAT, x=$FLOAT, y=$FLOAT}"
        Map<String, DecimalType> json = MqttUtils.handleSimpleJsonMessage(message);
        String channelId;
        for (Entry<String, DecimalType> entry : json.entrySet()) {
            switch (entry.getKey()) {
                case "x":
                    channelId = CHANNEL_ROTATION_X;
                    break;
                case "y":
                    channelId = CHANNEL_ROTATION_Y;
                    break;
                case "z":
                    channelId = CHANNEL_ROTATION_Z;
                    break;
                default:
                    System.err.print("Unknown JSON key: " + entry.getKey());
                    continue;
            }
            updateState.accept(channelId, entry.getValue());
        }
    }

    public static void handleAcceleration(String message, UpdateState updateState) {
        // state has the form "{z=$FLOAT, x=$FLOAT, y=$FLOAT}"
        Map<String, DecimalType> json = MqttUtils.handleSimpleJsonMessage(message);
        String channelId;
        for (Entry<String, DecimalType> entry : json.entrySet()) {
            switch (entry.getKey()) {
                case "x":
                    channelId = CHANNEL_ACCELERATION_X;
                    break;
                case "y":
                    channelId = CHANNEL_ACCELERATION_Y;
                    break;
                case "z":
                    channelId = CHANNEL_ACCELERATION_Z;
                    break;
                default:
                    System.err.print("Unknown JSON key: " + entry.getKey());
                    continue;
            }
            updateState.accept(channelId, entry.getValue());
        }
    }

}
