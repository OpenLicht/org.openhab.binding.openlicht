package org.openhab.binding.openlicht.handler;

import org.eclipse.smarthome.core.thing.Thing;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;

public class SamsungS6Handler extends AbstractSmartphoneHandler {

    public SamsungS6Handler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing, configurationHolder);
    }

}
