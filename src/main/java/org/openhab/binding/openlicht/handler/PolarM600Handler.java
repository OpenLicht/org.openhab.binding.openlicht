package org.openhab.binding.openlicht.handler;

import org.eclipse.smarthome.core.thing.Thing;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;

public class PolarM600Handler extends AbstractSmartWatchHandler {

    public PolarM600Handler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing, configurationHolder);
    }

    @Override
    protected String getSubTopic() {
        return "polar";
    }

}
