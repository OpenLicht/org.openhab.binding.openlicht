/**
 * Copyright (c) 2010-2018 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.openlicht.handler;

import static org.openhab.binding.openlicht.BindingConstants.CHANNEL_FLICK;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link SkyWriterHATHandler} is responsible for handling commands, which are
 * sent to the channel "$BASE_TOPIC/skywriter".
 *
 * @author René Schöne - Initial contribution
 */
public class SkyWriterHATHandler extends AbstractMqttHandler {

    private final @NonNull Logger logger = LoggerFactory.getLogger(SkyWriterHATHandler.class);

    public SkyWriterHATHandler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing, configurationHolder);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        logger.info("Got command for read-only thing: {} {}", channelUID.getAsString(), command.toFullString());
    }

    @Override
    public void processMessage(String topic, byte[] payload) {
        try {
            String state = new String(payload);
            updateState(CHANNEL_FLICK, StringType.valueOf(state));
        } catch (Exception e) {
            this.logger.warn("Could not process message", e);
        }
    }

    @Override
    protected String getSubTopic() {
        return "skywriter";
    }

}
