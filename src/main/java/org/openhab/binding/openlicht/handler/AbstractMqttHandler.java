package org.openhab.binding.openlicht.handler;

import static org.openhab.binding.openlicht.BindingConstants.*;
import static org.openhab.binding.openlicht.handler.HandlerPhase.*;

import java.math.BigDecimal;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.binding.mqtt.handler.AbstractBrokerHandler;
import org.eclipse.smarthome.core.common.registry.RegistryChangeListener;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingRegistry;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.io.transport.mqtt.MqttBrokerConnection;
import org.eclipse.smarthome.io.transport.mqtt.MqttMessageSubscriber;
import org.eclipse.smarthome.io.transport.mqtt.MqttServiceObserver;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractMqttHandler extends BaseThingHandler
        implements MqttMessageSubscriber, MqttServiceObserver, RegistryChangeListener<Thing> {

    private final @NonNull Logger logger = LoggerFactory.getLogger(AbstractMqttHandler.class);
    private @Nullable ThingRegistry thingRegistry;
    // private @Nullable MqttService mqttService;
    private int usedTopicLength = 0;
    private MqttBrokerConnection currentBrokerConnection = null;
    private boolean warnNoBrokerConnection = true;
    protected @Nullable ScheduledExecutorService executor;
    private Version version;
    private Lock configUpdateLock;
    private String myBrokerName;
    protected boolean byteBaseMessages;

    public AbstractMqttHandler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing);
        this.thingRegistry = configurationHolder.getThingRegistry();
        if (this.thingRegistry != null) {
            this.thingRegistry.addRegistryChangeListener(this);
        } else {
            logger.warn("No Thing registry, so no updated broker");
        }
        // this.mqttService = configurationHolder.getMqttService();
        // if (this.mqttService != null) {
        // this.mqttService.addBrokersListener(this);
        // } else {
        // logger.warn("No mqtt service, so no broker listener added");
        // }
        this.executor = configurationHolder.getExecutor();
        this.version = configurationHolder.getVersion();
        this.configUpdateLock = new ReentrantLock();
        logger.info("Started handler " + this.getClass().getSimpleName() + " in version " + version);
    }

    @Override
    public void added(Thing element) {
        if (thingIsMyMqttBroker(element)) {
            addConnectionOf(element);
        }
    }

    @Override
    public void removed(Thing element) {
        if (thingIsMyMqttBroker(element)) {
            removeMyBroker();
        }
    }

    @Override
    public void updated(Thing oldElement, Thing element) {
        if (thingIsMyMqttBroker(element)) {
            removeMyBroker();
            addConnectionOf(element);
        }
    }

    @Override
    public void brokerAdded(String brokerID, MqttBrokerConnection broker) {
        logger.debug("{} checking new broker '{}'", me(), brokerID);
        if (brokerID.equals(myBrokerName)) {
            myBrokerAdded(broker);
        }
    }

    @Override
    public void brokerRemoved(String brokerID, MqttBrokerConnection broker) {
        if (brokerID.equals(myBrokerName)) {
            removeMyBroker();
        }
    }

    private boolean thingIsMyMqttBroker(Thing thing) {
        if (myBrokerName == null) {
            logger.warn("Broker name of {} not set", me());
            return false;
        }
        if (thing == null) {
            return false;
        }
        if (!"mqtt:systemBroker".equals(thing.getThingTypeUID().getAsString())
                && !"mqtt:broker".equals(thing.getThingTypeUID().getAsString())) {
            return false;
        }
        return myBrokerName.equals(thing.getLabel());
    }

    private void addConnectionOf(Thing mqttBroker) {
        ThingHandler handler = mqttBroker.getHandler();
        // this.mqttService.addBrokerConnection(myBrokerName, currentBrokerConnection);
        if (handler instanceof AbstractBrokerHandler) {
            AbstractBrokerHandler abh = (AbstractBrokerHandler) handler;
            myBrokerAdded(abh.getConnection());
        }
    }

    @Override
    public void initialize() {
        statusUpdate(INIT, true, "Searching broker");
        new Thread(() -> {
            try {
                this.logger.debug("Initializing {}", me());
                this.configUpdateLock.lock();
                moreInitializeBefore();
                this.byteBaseMessages = getConfigValueAsBoolean(CONFIG_BYTE_BASED_MESSAGES);
                String brokerName = getConfigValueAsString(CONFIG_BROKER_NAME);
                if (brokerName == null) {
                    statusUpdate(CONFIGURATION, false, "BrokerName not set");
                    return;
                }
                this.myBrokerName = brokerName;
                logger.debug("Setting myBrokerName to '{}' for {}", myBrokerName, me());

                if (this.thingRegistry != null) {
                    for (Thing thing : this.thingRegistry.getAll()) {
                        if (thingIsMyMqttBroker(thing)) {
                            addConnectionOf(thing);
                        }
                    }
                } else {
                    logger.warn("No thing registry, thus no broker connection");
                }
                // MqttService service = this.mqttService;
                // if (service == null) {
                // logger.warn("No mqtt service yet for {}", me());
                // } else {
                // for (Entry<@NonNull String, @NonNull MqttBrokerConnection> entry : service.getAllBrokerConnections()
                // .entrySet()) {
                // brokerAdded(entry.getKey(), entry.getValue());
                // }
                // }
                if (this.currentBrokerConnection == null) {
                    statusUpdate(COMMUNICATION, false,
                            "Broker with name '" + this.myBrokerName + "' not found for " + me());
                }
                moreInitializeAfter();
                logger.debug("Finished initialization of {}", me());
            } finally {
                this.configUpdateLock.unlock();
            }
        }).start();
    }

    protected String getConfigValueAsString(String key) {
        return (String) getThing().getConfiguration().get(key);
    }

    protected Integer getConfigValueAsInteger(String key) {
        BigDecimal bd = (BigDecimal) getThing().getConfiguration().get(key);
        return bd == null ? null : bd.intValue();
    }

    protected boolean getConfigValueAsBoolean(String key) {
        return (boolean) getThing().getConfiguration().get(key);
    }

    public void myBrokerAdded(MqttBrokerConnection broker) {
        this.logger.info("Got correct broker {}, subscribing to topic {} for {}", broker, getTopic(), me());
        currentBrokerConnection = broker;
        warnNoBrokerConnection = true;
        updateTopicLength();
        String description = "Subscribing to topic " + getTopic() + " at " + broker;
        statusUpdate(CONFIGURATION, true, description);
        currentBrokerConnection.start();
        if (subscribeTopic()) {
            broker.subscribe(getTopic(), this).whenComplete((result, exception) -> {
                if (exception != null) {
                    this.logger.warn("exception", exception);
                    statusUpdate(COMMUNICATION, false, exception.getMessage());
                    return;
                }
                this.logger.info("Broker found for {}, thing is online", me());
                statusUpdate(FINISH, true, "Finished");
            });
        }
    }

    private void updateTopicLength() {
        // if subscribed to subtopics, a "/#" is added
        // then, we want only the part up to the last "/"
        usedTopicLength = getTopic().length() - (subscribeSubTopics() ? 1 : 0);
    }

    public void removeMyBroker() {
        statusUpdate(COMMUNICATION, false, "Broker is offline");
        try {
            if (currentBrokerConnection != null) {
                currentBrokerConnection.unsubscribe(getTopic(), this);
            }
        } finally {
            currentBrokerConnection = null;
            logger.debug("Removing broker of {}", me());
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        logger.debug("Disposing " + this);
        currentBrokerConnection = null;
    }

    protected void publish(String topic, String message) {
        if (currentBrokerConnection == null) {
            if (warnNoBrokerConnection) {
                // just report once
                logger.warn("Can't publish message in {}, no connection", me());
                warnNoBrokerConnection = false;
            }
            return;
        }
        // String topic = subscribeSubTopics() ? (getTopic().substring(0, usedTopicLength) + "out")
        // : (getTopic() + "/out");
        byte[] payload = message.getBytes();
        currentBrokerConnection.publish(topic, payload);
    }

    public final String getTopic() {
        return getConfigValueAsString(CONFIG_BASE_TOPIC) + (getSubTopic() == null ? "" : "/" + getSubTopic())
                + (subscribeSubTopics() ? "/#" : "");
    }

    protected final int getTopicLength() {
        return this.usedTopicLength;
    }

    protected String me() {
        return this.getClass().getSimpleName() + ":" + getThing().getUID().getAsString();
    }

    /**
     * Updates the state of the managed thing. Subclasses may override this to add special handling.
     *
     * @param phase       Phase of the thing configuration
     * @param success     Whether the phase was successful
     * @param description Detailed description of the status
     */
    protected void statusUpdate(HandlerPhase phase, boolean success, String description) {
        switch (phase) {
            case INIT:
                updateStatus(ThingStatus.UNKNOWN, ThingStatusDetail.CONFIGURATION_PENDING, description);
                break;
            case CONFIGURATION:
                if (success) {
                    updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_PENDING, description);
                } else {
                    updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR, description);
                }
                break;
            case COMMUNICATION:
                if (success) {
                    updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_PENDING, description);
                } else {
                    updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.COMMUNICATION_ERROR, description);
                }
                break;
            case FINISH:
                updateStatus(ThingStatus.ONLINE);
                break;
        }
    }

    /**
     * Subclasses may override this to do more asynchronous initialization before normal initialization.
     */
    protected void moreInitializeBefore() {
        // empty by default
    }

    /**
     * Subclasses may override this to do more asynchronous initialization after normal initialization.
     */
    protected void moreInitializeAfter() {
        // empty by default
    }

    /**
     * Subclasses may override this to subscribe to a special subtopic.
     * Do not add a leading nor a trailing slash!
     *
     * @return <code>null</code> by default to not use subtopics
     */
    protected String getSubTopic() {
        return null;
    }

    /**
     * Subclasses may override this to prevent topic subscription
     *
     * @return <code>true</code> by default
     */
    protected boolean subscribeTopic() {
        return true;
    }

    /**
     * Subclasses may override this to also subscribe to any topic sharing the same prefix.
     * This means, that an additional "<code>/#</code>" is added at the end of the topic.
     *
     * @return <code>true</code> by default
     */
    protected boolean subscribeSubTopics() {
        return true;
    }

    // /**
    // * Writes a float array from the given input based on the length of the targeted output.
    // *
    // * @param input the buffer to read from
    // * @param output the array to write in
    // */
    // protected static void writeFloatArray(ByteBuffer input, float[] output) {
    // for (int i = 0; i < output.length; i++) {
    // output[i] = input.getFloat();
    // }
    // }

}
