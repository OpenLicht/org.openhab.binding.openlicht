package org.openhab.binding.openlicht.handler;

public enum HandlerPhase {
    INIT,
    CONFIGURATION,
    COMMUNICATION,
    FINISH
}
