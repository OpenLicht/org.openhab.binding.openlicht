package org.openhab.binding.openlicht.handler;

import java.util.function.BiConsumer;

import org.eclipse.smarthome.core.types.State;

@FunctionalInterface
public interface UpdateState extends BiConsumer<String, State> {

}
