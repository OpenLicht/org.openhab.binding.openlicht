package org.openhab.binding.openlicht.handler;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.events.EventPublisher;
import org.eclipse.smarthome.core.items.GenericItem;
import org.eclipse.smarthome.core.items.GroupItem;
import org.eclipse.smarthome.core.items.Item;
import org.eclipse.smarthome.core.items.ItemNotFoundException;
import org.eclipse.smarthome.core.items.ItemRegistry;
import org.eclipse.smarthome.core.items.StateChangeListener;
import org.eclipse.smarthome.core.items.events.ItemEventFactory;
import org.eclipse.smarthome.core.library.types.DateTimeType;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.HSBType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.eclipse.smarthome.core.types.TypeParser;
import org.openhab.binding.openlicht.BindingConstants;
import org.openhab.binding.openlicht.internal.ConfigurationHolder;
import org.openhab.binding.openlicht.internal.DelegateEraserRegistryChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EraserHandler extends AbstractMqttHandler implements StateChangeListener {

    private final @NonNull Logger logger = LoggerFactory.getLogger(EraserHandler.class);
    private @Nullable ItemRegistry itemRegistry;
    private String outTopic;
    private boolean publishAll = false;
    private String publishGroupName;
    private boolean publishGroupFound = false;
    private Set<GenericItem> observedItems = new HashSet<>();
    private WeakReference<DelegateEraserRegistryChangeListener> delegateListener;
    private HandlerPhase lastStatusUpdatePhase;
    private boolean lastStatusUpdateSuccess;
    private String lastStatusUpdateDescription;
    private @Nullable EventPublisher eventPublisher;

    public EraserHandler(Thing thing, ConfigurationHolder configurationHolder) {
        super(thing, configurationHolder);
        this.itemRegistry = configurationHolder.getItemRegistry();
        this.eventPublisher = configurationHolder.getEventPublisher();
        ItemRegistry registry = this.itemRegistry;
        if (registry != null) {
            delegateListener = new WeakReference<DelegateEraserRegistryChangeListener>(
                    new DelegateEraserRegistryChangeListener(this));
            registry.addRegistryChangeListener(delegateListener.get());
        } else {
            logger.warn("No item registry set, so no updates for item changes");
        }
    }

    @Override
    protected void moreInitializeBefore() {
        this.outTopic = getConfigValueAsString(BindingConstants.CONFIG_ERASER_OUT_TOPIC);
        this.publishGroupName = getConfigValueAsString(BindingConstants.CONFIG_ERASER_PUBLISH_GROUP);
        this.publishAll = getConfigValueAsBoolean(BindingConstants.CONFIG_ERASER_PUBLISH_ALL);
        if (this.outTopic.charAt(this.outTopic.length() - 1) != '/') {
            this.outTopic += "/";
        }
        if (this.publishAll) {
            if (this.itemRegistry != null) {
                for (Item item : this.itemRegistry.getAll()) {
                    updated(item, item);
                }
            }
        } else {
            if (this.itemRegistry != null) {
                try {
                    Item publishGroupItem = this.itemRegistry.getItem(this.publishGroupName);
                    myGroupFound(publishGroupItem);
                } catch (ItemNotFoundException e) {
                    this.publishGroupFound = false;
                    logger.warn("Could not find group with name '" + this.publishGroupName + "'");
                }
            } else {
                logger.warn("No item registry set, so could not search for publish group");
            }
        }
    }

    private void myGroupFound(Item publishGroupItem) {
        if (publishGroupItem instanceof GroupItem) {
            this.publishGroupFound = true;
            GroupItem groupItem = (GroupItem) publishGroupItem;
            for (Item member : groupItem.getAllMembers()) {
                added(member);
            }
        } else {
            logger.warn("Found an item with name {}, but was not a generic item", this.publishGroupName);
        }
    }

    @Override
    protected void statusUpdate(HandlerPhase phase, boolean success, String description) {
        this.lastStatusUpdatePhase = phase;
        this.lastStatusUpdateSuccess = success;
        this.lastStatusUpdateDescription = description;
        if (this.publishAll) {
            super.statusUpdate(phase, success, description);
            return;
        }
        String newDescription = description;
        if (!this.publishGroupFound) {
            // append information to description
            newDescription = description + (success ? ", but" : " and") + " group to publish with name '"
                    + this.publishGroupName + "' not found.";
        }
        switch (phase) {
            case INIT:
                super.statusUpdate(phase, success, description);
                break;
            case CONFIGURATION:
            case COMMUNICATION:
                super.statusUpdate(phase, success, newDescription);
                break;
            case FINISH:
                if (this.publishGroupFound) {
                    super.statusUpdate(phase, success, description);
                } else {
                    super.statusUpdate(HandlerPhase.CONFIGURATION, false, newDescription);
                }
                break;
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        if (this.itemRegistry != null) {
            this.itemRegistry.removeRegistryChangeListener(delegateListener.get());
            delegateListener.clear();
        }
        for (GenericItem observedItem : observedItems) {
            observedItem.removeStateChangeListener(this);
        }
    }

    @Override
    public void processMessage(String topic, byte[] payload) {
        // check topic, and forward payload to respective item
        final String stringPayload = new String(payload);
        logger.debug("{} got message, in topc: '{}' with payload '{}'", this, topic, stringPayload);
        int indexOfLastSlash = topic.lastIndexOf('/');
        String itemName = topic.substring(indexOfLastSlash + 1);
        logger.debug("{} using item name: '{}'", this, itemName);
        ItemRegistry registry = this.itemRegistry;
        if (registry != null) {
            Item item;
            try {
                item = registry.getItem(itemName);
            } catch (ItemNotFoundException e) {
                logger.debug("Item with name '" + itemName + "' not found.");
                return;
            }
            // assume this is a GenericItem
            GenericItem genericItem = (GenericItem) item;
            // State newState = parseState(genericItem, new String(payload));
            Command command = TypeParser.parseCommand(genericItem.getAcceptedCommandTypes(), stringPayload);
            EventPublisher localEventPublisher = this.eventPublisher;
            if (command != null && localEventPublisher != null) {
                localEventPublisher.post(ItemEventFactory.createCommandEvent(itemName, command));
            } else {
                logger.debug(
                        "Could not create command for event-publisher {}, item {} with accecpted command types {} and payload {}",
                        localEventPublisher, itemName, genericItem.getAcceptedCommandTypes(), stringPayload);
                logger.debug("Trying to set state instead");
                State state = TypeParser.parseState(genericItem.getAcceptedDataTypes(), stringPayload);
                if (state != null) {
                    genericItem.setState(state);
                } else {
                    logger.debug("Could not create new state for item {} with accepted date types {} and payload {}",
                            itemName, genericItem.getAcceptedDataTypes(), stringPayload);
                }
            }
            // if (newState != null) {
            // logger.debug("Setting state {} of type {} for {}", newState, newState.getClass().getSimpleName(),
            // genericItem.getName());
            // genericItem.setState(newState);
            // } else {
            // logger.warn("Could not set state for {} using '{}'", genericItem, new String(payload));
            // }

        } else {
            logger.debug("No item registry to process message");
        }
    }

    private State parseState(GenericItem genericItem, String state) {
        for (Class<? extends @NonNull Command> type : genericItem.getAcceptedCommandTypes()) {
            try {
                if (type.equals(StringType.class)) {
                    return new StringType(state);
                } else if (type.equals(HSBType.class)) {
                    return new HSBType(state);
                } else if (type.equals(DecimalType.class)) {
                    return new DecimalType(state);
                } else if (type.equals(OnOffType.class)) {
                    return OnOffType.from(state);
                } else if (type.equals(DateTimeType.class)) {
                    return new DateTimeType(state);
                }
            } catch (IllegalArgumentException e) {
                // ignore the exception, and try next type
            }
        }
        return null;
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        logger.debug("got a command " + command);
    }

    public void added(Item element) {
        GenericItem genericItem = (GenericItem) element;
        if (this.publishAll) {
            if (observedItems.add(genericItem)) {
                genericItem.addStateChangeListener(this);
            }
        } else {
            if (!this.publishGroupFound && genericItem.getName().equals(this.publishGroupName)) {
                myGroupFound(genericItem);
                // update status with last known value (to remove the clause about missing group item)
                statusUpdate(lastStatusUpdatePhase, lastStatusUpdateSuccess, lastStatusUpdateDescription);
            }
            List<@NonNull String> groupNames = genericItem.getGroupNames();
            if (groupNames.contains(this.publishGroupName)) {
                if (observedItems.add(genericItem)) {
                    genericItem.addStateChangeListener(this);
                }
            }
        }
    }

    public void removed(Item element) {
        GenericItem genericItem = (GenericItem) element;
        if (observedItems.remove(genericItem)) {
            genericItem.removeStateChangeListener(this);
        }
        if (!this.publishAll && this.publishGroupFound && genericItem.getName().equals(this.publishGroupName)) {
            // someone evil deleted the publish group
            this.publishGroupFound = false;
            GroupItem groupItem = (GroupItem) genericItem;
            for (Item member : groupItem.getAllMembers()) {
                removed(member);
            }
        }
    }

    public void updated(Item oldElement, Item element) {
        removed(oldElement);
        added(element);
    }

    @Override
    public void stateChanged(Item item, State oldState, State newState) {
        logger.debug("Recevied an event for item: '" + item.getName() + "'");
        String topic = outTopic + item.getName();
        String payload = newState.toFullString();
        publish(topic, payload);
    }

    @Override
    public void stateUpdated(Item item, State state) {
        // ignored, as no change
    }

}
