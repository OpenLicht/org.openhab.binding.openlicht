/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.openlicht;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 * The {@link BindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author René Schöne - Initial contribution
 */
@NonNullByDefault
public class BindingConstants {

    private static final String BINDING_ID = "openlicht";

    // Configuration keys
    public static final String CONFIG_BROKER_NAME = "brokerName";
    public static final String CONFIG_BASE_TOPIC = "base-topic";
    public static final String CONFIG_BYTE_BASED_MESSAGES = "byte-based-messages";
    public static final String CONFIG_TIMEOUT_MQTT_UNSUPPORTED_CATEGORIES = "unsupported-category-reset";
    public static final String CONFIG_ERASER_OUT_TOPIC = "outTopic";
    public static final String CONFIG_ERASER_PUBLISH_GROUP = "publish-group";
    public static final String CONFIG_ERASER_PUBLISH_ALL = "publish-all";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_SKYWRITER_HAT = new ThingTypeUID(BINDING_ID, "skywriter-hat");
    public static final ThingTypeUID THING_TYPE_POLAR_M600 = new ThingTypeUID(BINDING_ID, "polar-m600");
    public static final ThingTypeUID THING_TYPE_MOTO_360 = new ThingTypeUID(BINDING_ID, "moto-360");
    public static final ThingTypeUID THING_TYPE_SAMSUNG_S6 = new ThingTypeUID(BINDING_ID, "samsung-s6");
    public static final ThingTypeUID THING_TYPE_ERASER = new ThingTypeUID(BINDING_ID, "eraser");

    // List of all Channel ids
    public static final String CHANNEL_FLICK = "flick";
    public static final String CHANNEL_ACCELERATION_X = "acceleration-x";
    public static final String CHANNEL_ACCELERATION_Y = "acceleration-y";
    public static final String CHANNEL_ACCELERATION_Z = "acceleration-z";
    public static final String CHANNEL_ROTATION_X = "rotation-x";
    public static final String CHANNEL_ROTATION_Y = "rotation-y";
    public static final String CHANNEL_ROTATION_Z = "rotation-z";
    public static final String CHANNEL_ACTIVITY = "activity";
    public static final String CHANNEL_HEART_RATE = "heart-rate";
    public static final String CHANNEL_STEPS = "steps";
    public static final String CHANNEL_BRIGHTNESS = "brightness";

    // List of MQTT categories
    public static final String MQTT_CATEGORY_FLICK = "flick";
    public static final String MQTT_CATEGORY_ACCELERATION = "acceleration";
    public static final String MQTT_CATEGORY_ROTATION = "rotation";
    public static final String MQTT_CATEGORY_ACTIVITY = "activity";
    public static final String MQTT_CATEGORY_HEART_RATE = "heart-rate";
    public static final String MQTT_CATEGORY_STEPS = "steps";
    public static final String MQTT_CATEGORY_BRIGHTNESS = "brightness";

    public static final String MQTT_BROKER_URL = "tcp://localhost:1883";
}
