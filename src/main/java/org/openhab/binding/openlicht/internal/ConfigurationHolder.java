package org.openhab.binding.openlicht.internal;

import java.util.concurrent.ScheduledExecutorService;

import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.events.EventPublisher;
import org.eclipse.smarthome.core.items.ItemRegistry;
import org.eclipse.smarthome.core.thing.ThingRegistry;
import org.eclipse.smarthome.io.transport.mqtt.MqttService;
import org.osgi.framework.Version;

public interface ConfigurationHolder {

    @Nullable
    ScheduledExecutorService getExecutor();

    @Nullable
    MqttService getMqttService();

    @Nullable
    ThingRegistry getThingRegistry();

    @Nullable
    ItemRegistry getItemRegistry();

    @Nullable
    EventPublisher getEventPublisher();

    Version getVersion();

}
