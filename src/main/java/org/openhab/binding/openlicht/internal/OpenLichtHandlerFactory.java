/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.openlicht.internal;

import static org.openhab.binding.openlicht.BindingConstants.*;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.events.EventPublisher;
import org.eclipse.smarthome.core.items.ItemRegistry;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingRegistry;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.thing.binding.ThingHandlerFactory;
import org.eclipse.smarthome.io.transport.mqtt.MqttService;
import org.openhab.binding.openlicht.handler.EraserHandler;
import org.openhab.binding.openlicht.handler.Moto360Handler;
import org.openhab.binding.openlicht.handler.PolarM600Handler;
import org.openhab.binding.openlicht.handler.SamsungS6Handler;
import org.openhab.binding.openlicht.handler.SkyWriterHATHandler;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.Version;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link OpenLichtHandlerFactory} is responsible for creating things and thing
 * handlers.
 *
 * @author René Schöne - Initial contribution
 */
@NonNullByDefault
@Component(configurationPid = "binding.openlicht", service = ThingHandlerFactory.class)
public class OpenLichtHandlerFactory extends BaseThingHandlerFactory implements ConfigurationHolder {

    private static final Logger logger = LoggerFactory.getLogger(OpenLichtHandlerFactory.class);
    private static final Set<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = Collections
            .unmodifiableSet(Stream.of(THING_TYPE_SKYWRITER_HAT, THING_TYPE_POLAR_M600, THING_TYPE_MOTO_360,
                    THING_TYPE_SAMSUNG_S6, THING_TYPE_ERASER).collect(Collectors.toSet()));
    private @Nullable MqttService service;
    private @Nullable ThingRegistry thingRegistry;
    private @Nullable ItemRegistry itemRegistry;
    private @Nullable ScheduledExecutorService executor;
    private @Nullable EventPublisher eventPublisher;

    @Override
    public boolean supportsThingType(ThingTypeUID thingTypeUID) {
        return SUPPORTED_THING_TYPES_UIDS.contains(thingTypeUID);
    }

    @Override
    protected @Nullable ThingHandler createHandler(Thing thing) {
        ThingTypeUID thingTypeUID = thing.getThingTypeUID();

        if (THING_TYPE_SKYWRITER_HAT.equals(thingTypeUID)) {
            return new SkyWriterHATHandler(thing, this);
        }
        if (THING_TYPE_POLAR_M600.equals(thingTypeUID)) {
            return new PolarM600Handler(thing, this);
        }
        if (THING_TYPE_MOTO_360.equals(thingTypeUID)) {
            return new Moto360Handler(thing, this);
        }
        if (THING_TYPE_SAMSUNG_S6.equals(thingTypeUID)) {
            return new SamsungS6Handler(thing, this);
        }
        if (THING_TYPE_ERASER.equals(thingTypeUID)) {
            return new EraserHandler(thing, this);
        }

        return null;
    }

    @Override
    protected void activate(ComponentContext componentContext) {
        super.activate(componentContext);
        executor = Executors.newScheduledThreadPool(2);
    }

    @Override
    protected void deactivate(ComponentContext componentContext) {
        super.deactivate(componentContext);
        if (executor != null) {
            executor.shutdown();
        }
    }

    @Override
    public @Nullable ScheduledExecutorService getExecutor() {
        return executor;
    }

    @Override
    public @Nullable MqttService getMqttService() {
        return service;
    }

    @Override
    public @Nullable ThingRegistry getThingRegistry() {
        return thingRegistry;
    }

    @Override
    public @Nullable ItemRegistry getItemRegistry() {
        return itemRegistry;
    }

    @Override
    public @Nullable EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Override
    public Version getVersion() {
        return FrameworkUtil.getBundle(getClass()).getVersion();
    }

    @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
    public void setMqttService(MqttService service) {
        logger.info("Setting mqtt service to {}", service);
        this.service = service;
    }

    public void unsetMqttService(MqttService service) {
        logger.info("Deleting mqtt service {}", service);
        this.service = null;
    }

    @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
    public void setThingRegistry(ThingRegistry thingRegistry) {
        logger.info("Setting thing registry to {}", thingRegistry);
        this.thingRegistry = thingRegistry;
    }

    public void unsetThingRegistry(ThingRegistry thingRegistry) {
        logger.info("Deleting thing registry {}", thingRegistry);
        this.thingRegistry = null;
    }

    @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
    public void setItemRegistry(ItemRegistry itemRegistry) {
        logger.info("Setting item registry to {}", itemRegistry);
        this.itemRegistry = itemRegistry;
    }

    public void unsetItemRegistry(ItemRegistry itemRegistry) {
        logger.info("Deleting item registry {}", itemRegistry);
        this.itemRegistry = null;
    }

    @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
    public void setEvemtPublisher(EventPublisher eventPublisher) {
        logger.info("Setting event publisher to {}", eventPublisher);
        this.eventPublisher = eventPublisher;
    }

    public void unsetEventPublisher(EventPublisher eventPublisher) {
        logger.info("Deleting event publisher {}", eventPublisher);
        this.eventPublisher = null;
    }
}
