/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.openlicht.internal;

/**
 * The {@link OpenLichtConfiguration} class contains fields mapping thing configuration parameters.
 *
 * @author René Schöne - Initial contribution
 */
public class OpenLichtConfiguration {

    /**
     * Name of the broker as defined in the name of the broker thing.
     */
    public String brokerName;

    /**
     * Base topic for publishing updates. Do not include a trailing slash.
     */
    public String baseTopic;

    /**
     * Interpret MQTT messages as raw bytes. If <code>false</code>, interpret the messages as Strings containing
     * numbers.
     */
    public boolean byteBasedMessages;

    /**
     * Timeout in seconds to log again unsupported MQTT categories. Set to zero to disable (default).
     */
    public int unsupportedCategoryResetTimeout;
}
