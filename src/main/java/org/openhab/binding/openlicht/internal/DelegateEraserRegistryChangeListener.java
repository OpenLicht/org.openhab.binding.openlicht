package org.openhab.binding.openlicht.internal;

import org.eclipse.smarthome.core.common.registry.RegistryChangeListener;
import org.eclipse.smarthome.core.items.Item;
import org.openhab.binding.openlicht.handler.EraserHandler;

public class DelegateEraserRegistryChangeListener implements RegistryChangeListener<Item> {

    private EraserHandler handler;

    public DelegateEraserRegistryChangeListener(EraserHandler handler) {
        this.handler = handler;
    }

    @Override
    public void added(Item element) {
        handler.added(element);
    }

    @Override
    public void removed(Item element) {
        handler.removed(element);
    }

    @Override
    public void updated(Item oldElement, Item element) {
        handler.updated(oldElement, element);
    }

}
